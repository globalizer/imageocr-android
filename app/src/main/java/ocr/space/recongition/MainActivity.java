package ocr.space.recongition;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import androidx.appcompat.app.AppCompatActivity;

/**
 * Created by suhasbachewar on 10/5/16.
 * Rewritten by Abandoned Cart on 2/25/20
 */

public class MainActivity extends AppCompatActivity {

    private final static String TAG = "ImageOCR";

    private String mAPiKey = "helloworld"; //TODO Add your own Registered API key
    private boolean isOverlayRequired;
    private String mImageUrl;
    private String mLanguage;
    private TextView mTxtResult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mImageUrl = "http://dl.a9t9.com/blog/ocr-online/screenshot.jpg"; // Image url to apply OCR API
        mLanguage = "eng"; //Language
        isOverlayRequired = true;
        init();

    }

    private void handleJSONResponse(JSONObject response) throws JSONException {
        if (response.has("ParsedText")) {
            JSONArray overlay = response.getJSONObject("TextOverlay")
                    .getJSONArray("Lines");
            Log.d(TAG, "TextOverlay returned " + overlay.length() + " Lines");
            for (int i = 0; i < overlay.length(); i++) {
                JSONArray words = overlay.getJSONObject(i).getJSONArray("Words");
                Log.d(TAG, "Line " + i + " returned " + words.length() + " Words");
                for (int x = 0; x < words.length(); x++) {
                    Log.d(TAG, "Line " + i + ", Word " + x + ": " +
                            words.getJSONObject(x).getString("WordText"));
                }
            }
            String text = response.getString("ParsedText");
            Log.d("ParsedText", text);
            mTxtResult.setText(text);
        } else if (response.has("ErrorMessage")) {
            String error = response.getString("ErrorMessage");
            Log.d("ErrorMessage", error);
            mTxtResult.setText(error);

        }
    }

    private void init() {
        new RequestImage().setListener(new RequestImage.RequestImageListener() {
            @Override
            public void onRequestImageFinished(Bitmap image) {
                ((ImageView) findViewById(R.id.source_img)).setImageBitmap(image);
            }
        }).execute(mImageUrl);
        mTxtResult = findViewById(R.id.actual_result);
        TextView btnCallAPI = findViewById(R.id.btn_call_api);

        if (btnCallAPI != null) {
            btnCallAPI.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    new OCRAsyncTask(MainActivity.this, mAPiKey, isOverlayRequired, mLanguage)
                            .setCallback(new OCRAsyncTask.OCRCallback() {
                        @Override
                        public void onOCRCallbackResults(JSONObject response) {
                            try {
                                handleJSONResponse(response);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }).execute(mImageUrl);
                }
            });
        }
    }
}
